package game;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Draggable;
import de.lessvoid.nifty.controls.Droppable;
import de.lessvoid.nifty.controls.DroppableDropFilter;
import de.lessvoid.nifty.controls.dragndrop.DroppableControl;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class GamePlayScreenController implements ScreenController {
	public GamePlayScreenController(GamePlay state) {
		this.state = state;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void bind(Nifty nifty, Screen screen) {
		this.nifty = nifty;
		this.screen = screen;
		
		this.screen.findControl("itemSlot1", DroppableControl.class).addFilter(new DroppableDropFilter() {
			@Override
			public boolean accept(Droppable source, Draggable draggable, Droppable target) {
				return state.getPlayer().filterDrop(source, draggable, target);
			}
		});
		
		this.screen.findControl("itemSlot2", DroppableControl.class).addFilter(new DroppableDropFilter() {
			@Override
			public boolean accept(Droppable source, Draggable draggable, Droppable target) {
				return state.getPlayer().filterDrop(source, draggable, target);
			}
		});
		
		this.screen.findControl("itemSlot3", DroppableControl.class).addFilter(new DroppableDropFilter() {
			@Override
			public boolean accept(Droppable source, Draggable draggable, Droppable target) {
				return state.getPlayer().filterDrop(source, draggable, target);
			}
		});
		
		this.screen.findControl("itemSlot4", DroppableControl.class).addFilter(new DroppableDropFilter() {
			@Override
			public boolean accept(Droppable source, Draggable draggable, Droppable target) {
				return state.getPlayer().filterDrop(source, draggable, target);
			}
		});
		
		this.screen.findControl("itemSlot5", DroppableControl.class).addFilter(new DroppableDropFilter() {
			@Override
			public boolean accept(Droppable source, Draggable draggable, Droppable target) {
				return state.getPlayer().filterDrop(source, draggable, target);
			}
		});
		
		this.screen.findControl("itemSlot6", DroppableControl.class).addFilter(new DroppableDropFilter() {
			@Override
			public boolean accept(Droppable source, Draggable draggable, Droppable target) {
				return state.getPlayer().filterDrop(source, draggable, target);
			}
		});
	}
	
	public void onInventoryClick(String elementId) {
		this.state.getPlayer().handleClick(elementId);
	}
	
	@Override
	public void onEndScreen() {
		
	}
	
	@Override
	public void onStartScreen() {
		
	}
	
	private GamePlay state;
	private Nifty nifty;
	private Screen screen;
}