package game;

import map.TileMap;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.opengl.SlickCallable;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import time.Schedule;
import character.Player;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.nulldevice.NullSoundDevice;
import de.lessvoid.nifty.slick2d.input.PlainSlickInputSystem;
import de.lessvoid.nifty.slick2d.input.SlickInputSystem;
import de.lessvoid.nifty.slick2d.render.SlickRenderDevice;
import de.lessvoid.nifty.spi.time.impl.AccurateTimeProvider;

public class GamePlay extends BasicGameState {	
	
	public GamePlay(){

	}
	
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		
		Input input = container.getInput();
		
		SlickInputSystem inputSys = new PlainSlickInputSystem();
		inputSys.setInput(input);
		
		this.nifty = new Nifty(new SlickRenderDevice(container), new NullSoundDevice(), inputSys, new AccurateTimeProvider());
		
		input.removeListener(this);
		input.removeListener(inputSys);
		input.addListener(inputSys);
		
		this.nifty.loadStyleFile("nifty-default-styles.xml");
		this.nifty.loadControlFile("nifty-default-controls.xml");
		this.nifty.registerScreenController(new GamePlayScreenController(this));
		this.nifty.fromXml("data/test.xml", "GamePlay");
        this.nifty.gotoScreen("GamePlay");		
		
		this.timeLabel = this.nifty.getScreen("GamePlay").findElementByName("timeLabel");
		this.cashLabel = this.nifty.getScreen("GamePlay").findElementByName("cashLabel");
		this.livesLabel = this.nifty.getScreen("GamePlay").findElementByName("livesLabel");
		this.attentionLabel = this.nifty.getScreen("GamePlay").findElementByName("attentionLabel");
		this.energyLabel = this.nifty.getScreen("GamePlay").findElementByName("energyLabel");
			
		try {
			this.map = new TileMap("default", this);
		} catch (SlickException e) {
			e.printStackTrace();
		}
		
		this.schedule = new Schedule();
		this.player = new Player("George", 30, 30, 30, new Image("assets/images/player.png"), this.map);
		
		this.paused = false;
	}
	
	public void render(GameContainer container, StateBasedGame game, Graphics graphics)
			throws SlickException {
		
		int translateX = -this.player.getX() + (container.getWidth() - this.player.getWidth()) / 2;
		int translateY = -this.player.getY() + (container.getHeight() - this.player.getHeight()) / 2;
		
		if(translateX > 0){
			translateX = 0;
		}
		
		if(translateY > 24){
			translateY = 24;
		}
		
		// TODO
		
		graphics.translate(translateX, translateY);
		
		this.map.draw();
		this.player.draw();
		
		graphics.translate(-translateX, -translateY);
		
		SlickCallable.enterSafeBlock();
		this.nifty.render(false);
        SlickCallable.leaveSafeBlock();
	}
	
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		
		Input input = container.getInput();
		
		this.nifty.update();
		
		if(!this.paused){
			this.player.update(delta);
			this.player.handleInput(input, delta);
			
			int translateX = this.player.getX() - container.getWidth() / 2;
			int translateY = this.player.getY() - container.getHeight() / 2;
			
			this.map.handleInput(input, (translateX > 0) ? translateX : 0, (translateY > 0) ? translateY : 0);
		}
		
		this.timeLabel.getRenderer(TextRenderer.class).setText(this.getTimeString());
		this.cashLabel.getRenderer(TextRenderer.class).setText(String.valueOf(this.player.getCash()));
		this.livesLabel.getRenderer(TextRenderer.class).setText(String.valueOf(this.player.getHp()));
		this.attentionLabel.getRenderer(TextRenderer.class).setText(String.valueOf(this.player.getAttention()) + "%");
		this.energyLabel.getRenderer(TextRenderer.class).setText(String.valueOf(this.player.getEnergy()) + "%");
	}

	public void pause() {
		this.paused = true;
		this.schedule.pause();
	}
	
	public void resume() {
		this.paused = false;
		this.schedule.resume();
	}
	
	public String getTimeString() {
		return this.schedule.asString();
	}
	
	public boolean isPaused() {
		return this.paused;
	}
	
	public Nifty getNifty() {
		return this.nifty;
	}
	
	public Player getPlayer() {
		return this.player;
	}
	
	public Schedule getSchedule() {
		return this.schedule;
	}
	
	public TileMap getMap() {
		return this.map;
	}
	
	public int getID() {
		return 0;
	}
	
	private TileMap map;
	private Player player;
	private Schedule schedule;
	private Nifty nifty;
	private Element timeLabel;
	private Element cashLabel;
	private Element livesLabel;
	private Element attentionLabel;
	private Element energyLabel;
	private boolean paused;
}