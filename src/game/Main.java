package game;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class Main extends StateBasedGame {
	public static final String gameName = "New RPG";
	public static final int play = 0;
	public static final int xSize = 960;
	public static final int ySize = 640;
	
	public Main(String gameName) {
		super(gameName);
		
		this.addState(new GamePlay());
	}
	
	public void initStatesList(GameContainer container) throws SlickException {
		this.getState(play).init(container, this);
		this.enterState(play);
	}
	   
	public static void main(String[] args) {
		AppGameContainer container;
		
		try {
			container = new AppGameContainer(new Main(gameName));
			container.setDisplayMode(xSize, ySize, false);
			container.setTargetFrameRate(60);
			container.setShowFPS(false);
			container.start();
		} catch(SlickException e){
			e.printStackTrace();
		}
	}
}