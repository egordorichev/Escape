package map;

import game.GamePlay;

import java.util.Random;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

public class Tile {
	public final static int size = 32;
	
	public enum Type {
		NONE, FLOOR, WALL, DOOR, TABLE, TOILET, BED_TOP, BED_BOTTOM, GRASS, DIRT // TODO: add more tiles!
	}
	
	public Tile(TileInfo info, int x, int y) { // So x and y are in tiles!
		this.id = info.getId();
		this.type = info.getType();
		this.x = x;
		this.y = y;
		this.variant = 1;// new Random().nextInt(3) + 1;

		this.texture = info.getTexture();
	}

	public void draw() {
		if(this.type != Tile.Type.NONE){
			this.texture.draw(this.x * Tile.size, this.y * Tile.size);
		}
	}
	
	public void onClick(GamePlay state) {
		
	}
	
	public boolean isColliding(Rectangle rect){
		if(this.type != Tile.Type.NONE){
			return new Rectangle(this.x * Tile.size, this.y * Tile.size, 
					Tile.size, Tile.size).intersects(rect);
		}
		
		return false;
	}
	
	public int getId() {
		return this.id;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public Type getType() {
		return this.type;
	}
	
	protected int id;
	protected int variant;
	protected int x;
	protected int y;
	protected Type type;
	protected Image texture;
}