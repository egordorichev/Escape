package map;

import game.GamePlay;
import item.Item;
import item.ItemInfo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

import de.lessvoid.nifty.Nifty;

public class TileMap {	
	public TileMap(String name, GamePlay state) throws SlickException {
		this.name = name;
		this.state = state;
		
		this.tileSprites = new SpriteSheet(new Image("assets/images/tiles.png"), 32, 32);
		
		this.tiles = new HashMap <Integer, TileInfo> ();
		this.items = new HashMap <Integer, ItemInfo> ();
		
		this.tiles.put(0, new TileInfo(0, Tile.Type.NONE, 
				this.tileSprites.getSubImage(0, 0)));
		
		this.tiles.put(1, new TileInfo(1, Tile.Type.FLOOR, 
				this.tileSprites.getSubImage(0, 0)));
		
		this.tiles.put(2, new TileInfo(2, Tile.Type.WALL, 
				this.tileSprites.getSubImage(1, 0)));
		
		this.tiles.put(3, new TileInfo(3, Tile.Type.WALL, 
				this.tileSprites.getSubImage(2, 0)));
		
		this.tiles.put(4, new TileInfo(4, Tile.Type.WALL, 
				this.tileSprites.getSubImage(3, 0)));
		
		this.tiles.put(5, new TileInfo(5, Tile.Type.WALL, 
				this.tileSprites.getSubImage(4, 0)));
		
		this.tiles.put(6, new TileInfo(6, Tile.Type.WALL, 
				this.tileSprites.getSubImage(5, 0)));
		
		this.tiles.put(7, new TileInfo(7, Tile.Type.WALL, 
				this.tileSprites.getSubImage(6, 0)));
		
		this.tiles.put(8, new TileInfo(8, Tile.Type.WALL, 
				this.tileSprites.getSubImage(7, 0)));
		
		this.tiles.put(9, new TileInfo(9, Tile.Type.WALL, 
				this.tileSprites.getSubImage(8, 0)));
		
		this.tiles.put(10, new TileInfo(10, Tile.Type.WALL, 
				this.tileSprites.getSubImage(9, 0)));
		
		this.tiles.put(11, new TileInfo(11, Tile.Type.DOOR, 
				this.tileSprites.getSubImage(10, 0)));
		
		this.tiles.put(12, new TileInfo(12, Tile.Type.TABLE, 
				this.tileSprites.getSubImage(0, 1)));
		
		this.tiles.put(13, new TileInfo(13, Tile.Type.BED_TOP, 
				this.tileSprites.getSubImage(1, 1)));
		
		this.tiles.put(14, new TileInfo(14, Tile.Type.BED_BOTTOM, 
				this.tileSprites.getSubImage(2, 1)));
		
		this.tiles.put(15, new TileInfo(15, Tile.Type.TOILET, 
				this.tileSprites.getSubImage(3, 1)));
		
		this.tiles.put(16, new TileInfo(16, Tile.Type.TOILET, 
				this.tileSprites.getSubImage(4, 1)));
		
		
		
		this.items.put(1, new ItemInfo(1, Item.Type.DEFAULT, "Wood", 
				"assets/images/items/wood.png"));
	
		this.load("data/maps/" + name + ".map");
	}
	
	public void draw() {
		for(int i = 0; i < 3; i++){
			for(int x = 0; x < this.width; x++){
				for(int y = 0; y < this.height; y++){
					this.map[i][x][y].draw();
				}
			}
		}
	}
	
	public void handleInput(Input input, int translateX, int translateY) {
		if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){			
			int x = (translateX + input.getMouseX()) / Tile.size;
			int y = (translateY + input.getMouseY() - 24) / Tile.size;
			
			System.out.println("update");
			
			if(x < this.width && y < this.height){
				this.map[2][x][y].onClick(this.state);
			}
		}
	}
	
	public boolean isColliding(Rectangle rect) {				
		for(int x = (int)(rect.getX()) / Tile.size - 1; 
				x < (int)(rect.getX() + rect.getWidth()) / Tile.size + 1; x++){
			
			for(int y = (int)(rect.getY()) / Tile.size - 1; 
					y < (int)(rect.getY() + rect.getHeight()) / Tile.size + 1; y++){
							
				Tile tile = this.getTile(x, y);

				if(tile != null && tile.isColliding(rect)){					
					return true;
				}
			} 
		} 
		
		return false;
	}
	
	public ItemInfo getItemInfo(int id) {
		return this.items.get(id);
	}
	
	public Item getItem(int id) {
		ItemInfo info = this.items.get(id);
		
		switch(info.getType()){
			case NONE: case DEFAULT: default: return new Item(info);
		}
	}
	
	public Tile getTile(int x, int y) {
		if(x < this.width && x >= 0 && y < this.height && y >= 0){
			return this.map[2][x][y];
		}
		
		return null;
	}
	
	public Nifty getNifty() {
		return this.state.getNifty();
	}
	
	private void load(String path) {
		try {
			String line = null;
			
			FileReader fileReader = new FileReader(path);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			ArrayList <ArrayList <Integer>>[] tempLayout = new ArrayList[3];
			
			for(int i = 0; i < 3; i++){
				tempLayout[i] = new ArrayList <>();
			}
			
			int layer = 0;
			
			while((line = bufferedReader.readLine()) != null) {
				if(line.isEmpty()){
					continue;
				}
				
				if(line.charAt(0) == '#'){
					layer++;
				} else {				
					ArrayList <Integer> row = new ArrayList();
					String[] values = line.trim().split(" ");
					
					for(String value : values){
						int id = Integer.parseInt(value);
						row.add(id);
					}
					
					tempLayout[layer].add(row);
				}
			}			
			
			bufferedReader.close();         

			
			this.width = tempLayout[0].get(0).size();
			this.height = tempLayout[0].size();
			
			this.map = new Tile[3][this.width][this.height];
			
			for(int i = 0; i < 3; i++){
				for(int x = 0; x < this.width; x++){
					for(int y = 0; y < this.height; y++){
						int id = tempLayout[i].get(y).get(x);
						TileInfo info = this.tiles.get(id);
	
						switch(info.getType()){
							case DOOR:
								this.map[i][x][y] = new Door(info, x, y);
							break;
							case BED_TOP: case BED_BOTTOM:
								this.map[i][x][y] = new Bed(info, x, y);
							break;
							default:
								this.map[i][x][y] = new Tile(info, x, y);
							break;
						}
					}
				}
			}
		} catch(FileNotFoundException exception) {
			System.out.println("Unable to open file '" + path + "'"); 
		} catch (IOException exception) {
			System.out.println("Error reading file '" + path + "'");
			exception.printStackTrace();
		}
	}
	
	private int width;
	private int height;
	private String name;
	private SpriteSheet tileSprites;
	private Tile[][][] map;
	private Map <Integer, TileInfo> tiles; 
	private Map <Integer, ItemInfo> items;
	private GamePlay state;
}