package map;

import game.GamePlay;

import org.newdawn.slick.geom.Rectangle;

public class Door extends Tile {

	public Door(TileInfo info, int x, int y) {
		super(info, x, y);
		
		this.opened = false;
		this.locked = false;
		this.timerStarted = false;
	}
	
	public void draw() {
		if(!this.opened){
			this.texture.draw(this.x * Tile.size, this.y * Tile.size);
		}
	}
	
	public void onClick(GamePlay state) {
		if(this.opened) {
			this.close();
		}
	}
	
	public void open(){
		this.opened = true;
		
		if(!this.timerStarted){
			this.timerStarted = true;
			
			new java.util.Timer().schedule(new java.util.TimerTask() {
				@Override
				public void run() {
					close();
					timerStarted = false;
				}
			}, 500);
		}
	}
	
	public void close(){
		this.opened = false;
	}
	
	public boolean isOpen(){
		return this.opened;
	}
	
	public boolean isColliding(Rectangle rect){
		if(!this.locked){
			return false;
		}
		
		return true;
	}
	
	private boolean opened;
	private boolean locked;
	private boolean timerStarted;

}
