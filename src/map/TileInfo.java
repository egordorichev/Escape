package map;

// import java.util.function.BiPredicate;
import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;

public class TileInfo {
	public TileInfo(int Id, Tile.Type type, Image texture) {
		this.id = id;
		this.type = type;
		this.texture = texture;
	}	
	
	public int getId() {
		return this.id;
	}
	
	public Tile.Type getType() {
		return this.type;
	}
	
	public Image getTexture() {
		return this.texture;
	}	
	
	private int id;
	private Tile.Type type;
	private Image texture;
}