package map;

import game.*;
import org.newdawn.slick.geom.Rectangle;

public class Bed extends Tile {
	public Bed(TileInfo info, int x, int y) {
		super(info, x, y);
	}

	public void onClick(GamePlay state) {
		
	}
	
	public void draw() {
		this.texture.draw(this.x * Tile.size, this.y * Tile.size);
	}
	
	public boolean isColliding(Rectangle rect){
		return new Rectangle(this.x * Tile.size, this.y * Tile.size, 
			Tile.size, Tile.size).intersects(rect);
	}
}
