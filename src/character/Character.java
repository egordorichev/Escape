package character;

import map.TileMap;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;

public class Character {
	public enum Direction {
		UP, RIGHT, DOWN, LEFT
	}
	
	public Character(String name, int strength, int speed, int intelligence, Image texture, TileMap map) {		
		this.name = name;
		this.strength = strength;
		this.speed = speed;
		this.intelligence = intelligence;
		this.texture = texture;
		this.direction = Character.Direction.RIGHT;
		this.x = 33;
		this.y = 33;
		this.map = map;
		this.maxHp = 50;
		this.hp = this.maxHp;
		
		this.runningUp = new Animation();
		
		this.runningUp.addFrame(this.texture.getSubImage(0, 36, 24, 36), 200);
		this.runningUp.addFrame(this.texture.getSubImage(24, 36, 24, 36), 200);
		this.runningUp.addFrame(this.texture.getSubImage(48, 36, 24, 36), 200);
		this.runningUp.addFrame(this.texture.getSubImage(72, 36, 24, 36), 200);
		
		this.runningRight = new Animation();
		
		this.runningRight.addFrame(this.texture.getSubImage(0, 72, 24, 36), 200);
		this.runningRight.addFrame(this.texture.getSubImage(24, 72, 24, 36), 200);
		this.runningRight.addFrame(this.texture.getSubImage(48, 72, 24, 36), 200);
		this.runningRight.addFrame(this.texture.getSubImage(72, 72, 24, 36), 200);
		
		this.runningDown = new Animation();
		
		this.runningDown.addFrame(this.texture.getSubImage(0, 0, 24, 36), 200);
		this.runningDown.addFrame(this.texture.getSubImage(24, 0, 24, 36), 200);
		this.runningDown.addFrame(this.texture.getSubImage(48, 0, 24, 36), 200);
		this.runningDown.addFrame(this.texture.getSubImage(72, 0, 24, 36), 200);
		
		this.runningLeft = new Animation();
		
		this.runningLeft.addFrame(this.texture.getSubImage(0, 108, 24, 36), 200);
		this.runningLeft.addFrame(this.texture.getSubImage(24, 108, 24, 36), 200);
		this.runningLeft.addFrame(this.texture.getSubImage(48, 108, 24, 36), 200);
		this.runningLeft.addFrame(this.texture.getSubImage(72, 108, 24, 36), 200);
		
		this.currentAnimation = this.runningRight;
	}
	
	public void draw() {
		this.currentAnimation.draw(this.x, this.y, this.getWidth(), this.getHeight());
	}
	
	public void update(int delta) {		

	}
	
	public void setDirection(Character.Direction direction) {
		this.direction = direction;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getStrength() {
		return this.strength;
	}
	
	public int getSpeed() {
		return this.speed;
	}
	
	public int getIntelligence() {
		return this.intelligence;
	}
	
	public Image getTexture() {
		return this.texture;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public Character.Direction getDirection() {
		return this.direction;
	}
	
	public int getWidth() {
		return 16;
	}
	
	public int getHeight() {
		return 24;
	}
		
	public int getHp() {
		return this.hp;
	}
	
	public int getMaxHp() {
		return this.maxHp;
	}
	
	protected Character.Direction direction;
	protected String name;
	protected Image texture;
	protected TileMap map;
	protected int strength;
	protected int speed;
	protected int intelligence;
	protected int x;
	protected int y;
	protected final static float movementSpeed = 0.15f;
	protected int hp;
	protected int maxHp;
	
	protected Animation currentAnimation;
	protected Animation runningUp;
	protected Animation runningRight;
	protected Animation runningDown;
	protected Animation runningLeft;
	
}