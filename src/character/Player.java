package character;

import item.Item;
import map.Door;
import map.Tile;
import map.TileMap;

import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;

import de.lessvoid.nifty.controls.Draggable;
import de.lessvoid.nifty.controls.Droppable;
import de.lessvoid.nifty.elements.render.PanelRenderer;
import de.lessvoid.nifty.tools.Color;

public class Player extends Character {
	public Player(String name, int strength, int speed, int intelligence,
			Image texture, TileMap map) {
		
		super(name, strength, speed, intelligence, texture, map);
		
		this.cash = 0;
		this.energy = 0;
		this.attention = 0;
		this.activeSlot = 0;
		
		this.inventory = new Item[8];
		
		this.inventory[2] = this.map.getItem(1);
		this.inventory[2].bind(this.map.getNifty(), "itemHolderImage3");
	
		this.inventory[1] = this.map.getItem(1);
		this.inventory[1].bind(this.map.getNifty(), "itemHolderImage2");
	}
	
	public void update(int delta) {
		
	}
	
	public void draw() {
		this.currentAnimation.draw(this.x, this.y, this.getWidth(), this.getHeight());
	}
	
	public void handleInput(Input input) {
		
	}
	
	public boolean filterDrop(Droppable source, Draggable draggable, Droppable target) {
		int targetId = Integer.parseInt(target.getId().replace("itemSlot", "")) - 1;
		
		if(this.inventory[targetId] == null) {
			int sourceId = Integer.parseInt(source.getId().replace("itemSlot", "")) - 1;
			
			this.inventory[targetId] = this.inventory[sourceId];
			this.inventory[sourceId] = null;
			
			this.applyActiveSlot(targetId);
			
			return true;
		}
		
		return false;
	}
	
	public void handleClick(String elementId) {		
		this.applyActiveSlot(Integer.parseInt(elementId.replace("itemHolder", "")) - 1);
	}

	public void handleInput(Input input, int delta) {
		int mX = 0;
		int mY = 0;
		
		if(input.isKeyDown(Input.KEY_W)) {				
			this.direction = Character.Direction.UP;
			this.currentAnimation = this.runningUp;
			
			mY -= this.movementSpeed * delta;
		} 

		if(input.isKeyDown(Input.KEY_A)){
			this.direction = Character.Direction.LEFT;
			this.currentAnimation = this.runningLeft;
			
			mX -= this.movementSpeed * delta;
		} 

		if(input.isKeyDown(Input.KEY_D)){
			this.direction = Character.Direction.RIGHT;
			this.currentAnimation = this.runningRight;
			
			mX += this.movementSpeed * delta;
		} 

		if(input.isKeyDown(Input.KEY_S)){
			this.direction = Character.Direction.DOWN;
			this.currentAnimation = this.runningDown;
			
			mY += this.movementSpeed * delta;
		} 
		
		if(!this.map.isColliding(new Rectangle(this.x + mX, 
				this.y, this.getWidth(), this.getHeight()))){
			
			this.x += mX;
		}
		
		if(!this.map.isColliding(new Rectangle(this.x, 
				this.y + mY, this.getWidth(), this.getHeight()))){
			
			this.y += mY;
		}
		
		Tile tile = this.map.getTile(this.x / Tile.size, this.y / Tile.size);
		
		if(tile != null && tile.getType() == Tile.Type.DOOR){
			((Door)tile).open();
		}
	} 
	
	public int getCash() {
		return this.cash;
	}
	
	public int getEnergy() {
		return this.energy;
	}
	
	public int getAttention() {
		return this.attention;
	}
	
	private void applyActiveSlot(int slot) {
		this.map.getNifty().getCurrentScreen().findElementById("itemSlot" + (this.activeSlot + 1)).getRenderer(PanelRenderer.class).setBackgroundColor(new Color("#606060"));
		this.activeSlot = slot;
		this.map.getNifty().getCurrentScreen().findElementById("itemSlot" + (this.activeSlot + 1)).getRenderer(PanelRenderer.class).setBackgroundColor(new Color("#222f"));
	}
	
	private int cash;
	private int energy;
	private int attention;
	private Item[] inventory;
	private int activeSlot;
}