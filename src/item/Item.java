package item;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.render.NiftyImage;

public class Item {
	public enum Type {
		NONE, DEFAULT, TILE, WEAPON, CLOTHES
	};
	
	public Item(ItemInfo info) {
		this.info = info;
	}
	
	public void onRightClick() {
		
	}
	
	public void onLeftClick() {
		
	}
	
	public void bind(Nifty nifty, String elementId) {
		NiftyImage image = nifty.getRenderEngine().createImage(nifty.getCurrentScreen(), this.info.getTexture(),  false);
		Element element = nifty.getCurrentScreen().findElementByName(elementId);
		
		element.getRenderer(ImageRenderer.class).setImage(image);
	
		this.element = element;
	}
	
	public Type getType() {
		return this.info.getType();
	}
	
	public String getTexture() {
		return this.info.getTexture();
	}
	
	public Element getElement() {
		return this.element;
	}
	
	private ItemInfo info;
	private Element element;
}
