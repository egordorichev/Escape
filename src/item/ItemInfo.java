package item;


public class ItemInfo {	
	public ItemInfo(int id, Item.Type type, String name, String texture) {
		this.id = id;
		this.type = type;
		this.name = name;
		this.texture = texture;
	}
	
	public int getId() {
		return this.id;
	} 
	
	public Item.Type getType() {
		return this.type;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getTexture() {
		return this.texture;
	}
	
	private Item.Type type;
	private int id;
	private String name;
	private String texture;
}