package time;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TimerTask;

import javax.swing.Timer;

public class Schedule {
	final static String schedule[] = {
		"Lights Out", "Lights Out",
		"Lights Out", "Lights Out",
		"Lights Out", "Lights Out",
		"Lights Out", "Lights Out",
		"Morning Roll Call", "Breakfast",
		"Work Period", "Work Period",
		"Work Period", "Exercise Reriod", 
		"Free Period", "Free Period", 
		"Meal time", "Work Period", 
		"Work Period", "Work Period", 
		"Shower Block", "Evening Roll Call", 
		"Lights Out", "Lights Out"
	};
	
	public Schedule() {
		this.hour = 8;
		this.minute = 0;
		this.paused = false;
		
		this.interval = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!paused){
					minute += 1;
					
					if(minute > 59){
						hour += 1;
						minute = 0;
					}
					
					if(hour > 24){
						hour = 0;
					}
				}
			}
		});
		
		this.runner = new java.util.Timer(false);
		this.runner.schedule(new TimerTask() {
			@Override
			public void run() {
				interval.start();
			}
			
		}, 0);
	}
	
	public void pause() {
		this.paused = true;
	}
	
	public void resume() {
		this.paused = false;
	}
	
	public String asString() {
		return this.getTime() + " - " + this.schedule[this.hour];
	}
	
	public String getTime() {
		String hourString = String.format("%02d", this.hour);
		String minuteString = String.format("%02d", this.minute);
		
		return hourString + ":" + minuteString;
	}
	
	public boolean isPaused() {
		return this.paused;
	}
	
	private int hour;
	private int minute;
	private boolean paused;
	private Timer interval;
	private java.util.Timer runner;
}